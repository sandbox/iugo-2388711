<?php
/**
 * config form.
 */
function jquery_formcache_config_form($form, &$form_state) {
  $form = array();
  
  $form['datasaver_forms_list_type'] = array(
    '#type' => 'radios',
    '#title' => t('Apply Formcache to specific forms'),
    '#default_value' => variable_get('datasaver_forms_list_type',1),
    '#options' => array(
    	'1' => t('All forms except those listed'),
    	'2' => t('Only the listed forms'),
    ),
  );
  
  $form['datasaver_forms_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Forms list'),
    '#default_value' => variable_get('datasaver_forms_list'),
    '#description' => t('Specify form IDs separated by a comma.'),
  );
    
 
  return system_settings_form($form);
}
?>