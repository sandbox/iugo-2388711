(function($) {
  Drupal.behaviors.jquery_formcache = {
    attach: function(context, settings) {


var s = Drupal.settings.jquery_formcache;


$('form').each(function(){
	var fid = $(this).attr('id');
	if($.inArray(fid, s.form_ids)!==-1){
		$(this).formcache();
}
});
    
    }
}
})(jQuery);